<!DOCTYPE html>
<html>
	<head>
		<title>{{ $title }}</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	</head>
	<style>
		body {background-image: linear-gradient(to right , #6B5E5B, #E1C699, #D9C2B3, #4E4A45);}
		#what-container {margin-top: 10rem;}
		.card {background-color: rgba(0,0,0,.45); padding: 1rem; color: white;}
	</style>
	<header>
		@include('partials.header')	
	</header>
	<body>
		
		<div class="container col-6 mx-auto" id="what-container">
			@yield('content')
		</div>
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</body>
	@include('partials.footer')
</html>