<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

	<a class="navbar-brand" href="menu.php">Laravel Store</a>
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse text-right" id="navbar">

		<ul class="navbar-nav mr-auto">

			<li class="nav-item">
				<a class="nav-link" href="../views/menu.php">Menu</a>
			</li>

			<li class="nav-item">

				<a class="nav-link" href="../views/cart.php">Cart

					<span id="span-cart-count" class="badge badge-light">

						<?php 
							if(isset($_SESSION['cart']) && count($_SESSION['cart'])) {
								echo array_sum($_SESSION['cart']);
							}
							else {
								echo '0';
							}

						?>


					</span>

				</a>

			</li>

		</ul>

		<ul class="navbar-nav ml-auto">

			<?php if (isset($_SESSION['username'])) : ?>

				<li class="nav-item">
					<a href="transactions.php" class="nav-link">Transactions</a>
				</li>

				<li class="nav-item">
					<a href="../controllers/logout.php" class="nav-link">Logout</a>
				</li>

				<?php else : ?>

					<li class="nav-item">
						<a href="login.php" class="nav-link">Login</a>
					</li>

					<li class="nav-item">
						<a href="register.php" class="nav-link">Register</a>
					</li>

				<?php endif; ?>

			</ul>
		</div>
	</nav>