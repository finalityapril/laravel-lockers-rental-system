import React, { Component } from 'react'


class Transactions extends Component {

	constructor(props) {
		super(props)

		this.state = {
			rentals: []
		}
	}

	componentWillMount() {
		this.getRent()
	}

	getRent() {
		fetch('/api/rental/' + this.props.userId)
		.then((response) => response.json())
		.then((rentals) => {
			console.log(rentals)
			this.setState({ rentals: rentals })
		})
	}

	pressedButton() {
		window.location.reload()
	}

	
	returnLocker(id) {
		let formData = new FormData()

		formData.append('_method', 'PUT')
		formData.append('admin_approved', 4)
		formData.append('rental_id', id)
		
		let payload = { 
			method: 'post', 
			body: formData 
		}
		fetch('/api/forreturn', payload)
		.then((response) => response.json())
		.then((data) => {
			this.setState({
				returnToTransactions: true
			})
		})
		alert('Returning this locker')
	}

	render() {
		if(this.state.returnToTransactions == true){
			this.pressedButton()
		}

		return(
			<div className="container-fluid mt-3">

				<h3 id="header-text" className="text-center">Transactions</h3>
				
				<table className="table table-bordered table-striped table-dark table-responsive-sm">
						
					<thead>
						<tr className="text-center">
							<th>User Name</th>
							<th>Order ID</th>
							<th>Item Name</th>
							<th>Date Created</th>
							<th>Approval Status</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						{ 
							this.state.rentals.map((rental) => {
								return <tr key={ rental.rental_id } className="text-center" >
									<td>{rental.name}</td>
									<td>{ rental.rental_id }</td>
									<td>{ rental.item_name}</td>
									<td>{ rental.created_at }</td>
									<td>{ 
										(rental.admin_approved==0) ? (<i>Pending Approval</i>) :
										(rental.admin_approved==1) ? (<div className="text-success">Approved - Now Using</div>) :
										(rental.admin_approved==2) ? (<div className="text-danger">Disapproved</div>) :
										(rental.admin_approved==3) ? (<div className="text-primary">Returned by Member</div>) :
										(rental.admin_approved==4) ? (<div className="text-warning font-italic">Pending Return</div>) :
										(<p>Request Invalid</p>) 
									}</td>
									<td>
										 {
										 	(rental.admin_approved==0 || rental.admin_approved==4) ? 
										 		(<p className="text-primary">Wait For Admin Action</p>) :
										 	(rental.admin_approved==1) ?
										 		(<button onClick={ () => this.returnLocker(rental.rental_id) } className="btn btn-success mx-2" id="history-btn">Return Locker</button>) :
										 	(<i>No Available Action</i>)
										 }
									</td>
								</tr>
							})
						}
					</tbody>

				</table>
				<br/><br/>

			</div>
		)
	}
}


export default Transactions