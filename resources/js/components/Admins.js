import React, { Component } from 'react'
import { Link } from 'react-router-dom'

const Admins = (props) => {

	let btnAddUser = (props.user.role == 'admin') ?
	
	<Link className="btn btn-sm btn-primary" to="/register"><b>+</b>New User</Link> : null 


	return(
		<div className="container-fluid mt-3">

			<h3 id="header-text">Users</h3>

			{ btnAddUser }

			<UserList role={ props.user.role } />

		</div>
	)
}


class UserList extends Component {

	constructor(props) {
		super(props)

		this.state = {
			users: []
		}
	}

	componentDidMount() {
		fetch('/api/admins')
		.then((response) => response.json())
		.then((users) => {
			this.setState({ users: users })	
		})
	}


	render() {

		return (

			<div className="row mt-3">
			{
				this.state.users.map((user) => {
					return (
						<div key={ user.id } className="col-3 mb-3">
							
							<div className="card h-100">

								<div className="card-body">

									<div className="form-group">

									<h4 className="card-title" id="user-text">{ user.name }</h4>
									</div>

									<p className="card-text"><b>Email:</b> { user.email }</p>		

									<p className="card-text"><b>Role:</b> { user.user_role }</p>	

									<p className="card-text"><b>Status:</b> 
									{ (user.is_archived == 0) ? (" Active") :
										(" User Archived")
									}

									</p>									
									{
										(this.props.role == 'admin') ?
										(
											<div className="btn-group btn-block">
											
												<Link className="btn btn-primary" to={ "/edit-user?id=" + user.id }>Edit</Link>
												<Link className="btn btn-danger" to={ "/delete-user?id=" + user.id }>Delete</Link>
										
											</div>
										):
										(
											<div> </div>
										)
									}
								</div>
							</div>
						</div>
					)
				})
			}
			</div>
		)
	}

}


export default Admins