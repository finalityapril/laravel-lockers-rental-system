import React, { Component, Fragment } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Footer from './Footer'
import Header from './Header'
import '../appstyle.css'

import Menu from './Menu'
import About from './About'
import Assets from './Assets'
import Login from './Login'
import Register from './Register'
import AddItem from './AddItem'
import Logout from './Logout'
import Transactions from './Transactions'
import History from './History'
import EditItem from './EditItem'
import DeleteItem from './DeleteItem'
import Users from './Users'
import EditUser from './EditUser'
import DeleteUser from './DeleteUser'
import ReactivateUser from './ReactivateUser'
import RequestItem from './RequestItem'


class App extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: localStorage.getItem('id'),
            name: localStorage.getItem('name'),
            role: localStorage.getItem('role'),
            now_booking: localStorage.getItem('now_booking'),
            assetsQuantity: 0
        }
    }

  
    componentWillMount() {  
        this.getTotalAssetsQuantity()
    }

    getTotalAssetsQuantity() {
        fetch('/assets/total-quantity')
        .then((response) => response.text())
        .then((quantity) => {
            this.setState({ assetsQuantity: quantity })
        })
    }

    emptyAssets() {
        fetch('/assets/empty')
        .then((response) => response.text())
        .then(() => {
            this.getTotalAssetsQuantity()
        }) 
    }

    setUser(user) {
        localStorage.setItem('id', user.id)
        localStorage.setItem('name', user.name)
        localStorage.setItem('role', user.user_role)
        localStorage.setItem('now_booking', user.now_booking)

        this.setState({
            id: localStorage.getItem('id'),
            name: localStorage.getItem('name'),
            role: localStorage.getItem('role'),
            now_booking: localStorage.getItem('now_booking')

        })
    }

    unsetUser(user) {
        localStorage.clear()

        this.setState({
            id: localStorage.getItem('id'),
            name: localStorage.getItem('name'),
            role: localStorage.getItem('role'),
            now_booking: localStorage.getItem('now_booking')

        })
    }

    render () {
        let MenuComponent = (props) => (<Menu {...props} user={ this.state } getTotalAssetsQuantity={ this.getTotalAssetsQuantity.bind(this) }/>)
        let LoginComponent = (props) => (<Login {...props} id={ this.state.id } setUser={ this.setUser.bind(this) }/>)
        let LogoutComponent = (props) => (<Logout {...props} unsetUser={ this.unsetUser.bind(this) } emptyAssets={ this.emptyAssets.bind(this) }/>)
        let AssetsComponent = (props) => (<Assets {...props} getTotalAssetsQuantity={ this.getTotalAssetsQuantity.bind(this) } emptyAssets={ this.emptyAssets.bind(this) } userId={ this.state.id }/>)
        let TransactionsComponent = (props) => (<Transactions {...props} userId={ this.state.id } userRole={this.state.role}/>)
        let HistoryComponent = (props) => (<History {...props} userId={ this.state.id } userRole={this.state.role}/>)
        let UsersComponent = (props) => (<Users {...props} user={ this.state } />)
        let AdminsComponent = (props) => (<Admins {...props} user={ this.state } />)
        let RequestItemComponent = (props) => (<RequestItem {...props} getTotalAssetsQuantity={ this.getTotalAssetsQuantity.bind(this) } emptyAssets={ this.emptyAssets.bind(this) } userId={ this.state.id } />)
        return (
            <Fragment>
                <BrowserRouter>
                    <Header id={ this.state.id } name={ this.state.name } role={ this.state.role } assetsQuantity={ this.state.assetsQuantity }/>
                    
                        <Switch>
                            <Route exact path="/" component={ About }/>
                            <Route exact path="/menu" render={ MenuComponent }/>
                            <Route exact path="/login" render={ LoginComponent }/>
                            <Route exact path="/logout" render= { LogoutComponent }/>
                            <Route exact path="/assets" render= { AssetsComponent }/>
                            <Route exact path="/register" component={ Register }/>
                            <Route exact path="/add-item" component={ AddItem }/>
                            <Route exact path="/edit-item" component={ EditItem }/>
                            <Route exact path="/delete-item" component={ DeleteItem }/>
                            <Route exact path="/request-item" render={ RequestItemComponent }/>
                            <Route exact path="/userlist" render={ UsersComponent }/>
                            <Route exact path="/edit-user" component={ EditUser }/>
                            <Route exact path="/delete-user" component={ DeleteUser }/>
                            <Route exact path="/reactivate-user" component={ ReactivateUser }/>
                            <Route exact path="/transactions" render={ TransactionsComponent }/>
                            <Route exact path="/history" render={ HistoryComponent }/>
                        </Switch>
                </BrowserRouter>
                <Footer/>
            </Fragment>
            )
    }
}


ReactDOM.render(<App/>, document.getElementById('app'));

//dbZzcvAXGo     05zZxDvPVq