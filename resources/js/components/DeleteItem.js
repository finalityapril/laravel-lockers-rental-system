import React, { Component } from 'react'
import queryString from 'query-string'
import { Link, Redirect } from 'react-router-dom'

const DeleteItem = (props) => (

	<div className="container-fluid mt-3">

	        <div className="row">

	            <div className="col-sm-6 mx-auto">

	                <h3 className="text-center mt-5" id="header-text">Delete Item</h3>

	                <div className="card">

	                    <div className="card-header">Item Information</div>

	                    <div className="card-body">

	                    	<DeleteItemForm urlParam = { props.location.search }/>

	                    </div>

	                </div>

	            </div>

	        </div>
	        <br/>
	    </div>
)

class DeleteItemForm extends Component {

	constructor(props) {
		super(props)

		let params = queryString.parse(this.props.urlParam)

		this.state = {
			itemId: params.id,
			itemName: '',
			description:'',
			categoryId: undefined,
			categories:[],
			statusId: undefined,
			category: '',
			status: '',
			statuses: [],
			returnToMenu:false
		}
	}

	componentWillMount() {
		fetch('/api/item/' + this.state.itemId)
		.then((response) => response.json())  
		.then((item) => {
			this.setState({ 
			 itemName: item.item_name,
			 description: item.description,
			 categoryId: item.category_id,
			 statusId: item.status_id,
			})
		})

		fetch('/api/categories')
		.then((response) => response.json())
		.then((categories)=> {
			this.setState({categories: categories})
		})

		fetch('/api/statuses')
		.then((response) => response.json())
		.then((statuses)=> {
		this.setState({statuses: statuses})
		})
	}


	formSubmitHandler(e){

		e.preventDefault();
		// create new form
		let formData = new FormData()
		// send through existing form to states
		formData.append('_method', 'DELETE')
		formData.append('id', this.state.itemId)

		let payload = { 
			method: 'post', 
			body: formData 
		}

		fetch('/api/item', payload)
		.then((response) => response.text())
		.then(() => {
			this.setState({
				returnToMenu: true
			})
		})
	}

	render() {

		if(this.state.returnToMenu){
			return <Redirect to='/menu'/>
		}
		return (

		<form onSubmit={ this.formSubmitHandler.bind(this) }>

			<div className="form-group">
				<label>Item Name</label>
				<input value={this.state.itemName} type="text"  className="form-control" readOnly/>
			</div>

			<div className="form-group">
				<label>Description</label>
				<input value={this.state.description} type="text"  className="form-control" readOnly/>
			</div>

			<div className="form-group">
				<label>Category</label>

				<select defaultValue className="form-control" value={this.state.categoryId}  readOnly>
				<option value disabled>Select Category</option>
				{
					this.state.categories.map((category) => {
						return <option key={category.id} value={ category.id }>{ category.category_name }</option>
					})
				}
				</select>
			</div>

			<div className="form-group">
				<label>Status</label>

				<select defaultValue className="form-control" value={this.state.statusId}  readOnly>
				<option value disabled>Select Status</option>
				{
					this.state.statuses.map((status) => {
						return <option key={status.id} value={ status.id }>{ status.status_name }</option>
					})
				}
				</select>
			</div>

			<button type="submit" className="btn btn-danger btn-block">Delete</button>
			<Link className="btn btn-success btn-block" to="/menu">Cancel</Link>

		</form>

		)
}

		
}


export default DeleteItem