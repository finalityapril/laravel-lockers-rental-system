import React, { Component } from 'react'
import { Link } from 'react-router-dom'

const Users = (props) => {

	let btnAddUser = (props.user.role == 'admin') ?
	
	<Link className="btn btn-sm btn-primary" to="/register"><b>+</b>New User</Link> : null 


	return(
		<div className="container-fluid mt-3">

			<h3 id="header-text">Users</h3>

			{ btnAddUser }

			<UserList role={ props.user.role } />
			<br/>

		</div>
	)
}


class UserList extends Component {

	constructor(props) {
		super(props)

		this.state = {
			users: []
		}
	}

	componentDidMount() {
		fetch('/api/users')
		.then((response) => response.json())
		.then((users) => {
			this.setState({ users: users })	
		})
	}


	render() {

		return (

			<div className="row mt-3">
			{
				this.state.users.map((user) => {
					return (
						<div key={ user.id } className="col-sm-3 mb-3">
							
							<div className="card mb-3">

								<div className="card-body">

									<div className="form-group">

										<h4 className="card-title" id="user-text">{ user.name }</h4>
									</div>

									<p className="card-text"><b>Email:</b> { user.email }</p>		

									<p className="card-text"><b>Role:</b> { user.user_role }</p>	

									<p className="card-text"><b>Status:</b> 
									{ (user.is_archived == 0) ? (<span className="text-success ml-1">ACTIVE</span>) :
										(<span className="text-danger ml-1">USER ARCHIVED</span>)
									}

									</p>									
									{
										(this.props.role == 'admin') ?
										(
											<div className="btn-group btn-block">								
												<Link className="btn btn-primary" to={ "/edit-user?id=" + user.id }>Edit</Link>

												{
													(user.is_archived == 0 ) ?
														(<Link className="btn btn-danger" to={ "/delete-user?id=" + user.id }>
														Delete</Link>) :
														(<Link className="btn btn-success" to={ "/reactivate-user?id=" + user.id }>
														Reactivate</Link>)
												}
										
											</div>
										):
										(
											<div> </div>
										)
									}
								</div>
							</div>
						</div>
					)
				})
			}
			</div>
		)
	}

}


export default Users