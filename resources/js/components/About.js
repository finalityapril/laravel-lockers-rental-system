import React from 'react'

const About = () => {
	return (
		<div>

			<div className='about-container'>
				<img src={require('../storage-space.jpg')} id="about-lockers" alt="lockers-banner"/>
				
				<div className="about-text">
					<h3>Need space?</h3>
					<p><span>Pop</span>Lockers offers various storage solutions for your organization's needs.</p>
				</div>
			</div>
		</div>
	)
}

export default About