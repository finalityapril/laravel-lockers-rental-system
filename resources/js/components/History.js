import React, { Component } from 'react'



class History extends Component {

	constructor(props) {
		super(props)

		this.state = {
			rentalalls: [],
		}
	}

	componentWillMount() {
		this.getRent()
	}

	getRent() {
		fetch('/api/rentals')
		.then((response) => response.json())
		.then((rentalalls) => {
			this.setState({ rentalalls: rentalalls })
			console.log(rentalalls)
		})
	}

	pressedButton() {
		window.location.reload()
	}

	approve(id) {
		let formData = new FormData()

		formData.append('_method', 'PUT')
		formData.append('admin_approved', 1)
		formData.append('rental_id', id)
		
		let payload = { 
			method: 'post', 
			body: formData 
		}
		fetch('/api/rentalok', payload)
		.then((response) => response.json())
		.then((data) => {
			this.setState({
				returnToTransactions: true
			})
		})
		alert('Member Request Approved')
	}

	deny(id) {
		let formData = new FormData()

		formData.append('_method', 'PUT')
		formData.append('admin_approved', 2)
		formData.append('rental_id', id)
		
		let payload = { 
			method: 'post', 
			body: formData 
		}
		fetch('/api/rentalno', payload)
		.then((response) => response.json())
		.then((data) => {
			this.setState({
				returnToTransactions: true
			})
		})
		alert('Member Request Denied')
	}

	tagReturned(id) {
		let formData = new FormData()

		formData.append('_method', 'PUT')
		formData.append('admin_approved', 3)
		formData.append('rental_id', id)
		
		let payload = { 
			method: 'post', 
			body: formData 
		}
		fetch('/api/rentalreturn', payload)
		.then((response) => response.json())
		.then((data) => {
			this.setState({
				returnToTransactions: true
			})
		})
		alert('Locker Tagged as Returned')
	}


	render() {
		if(this.state.returnToTransactions == true){
			this.pressedButton()
		}

		return(
			<div className="container-fluid mt-3">

				<h3 id="header-text" className="text-center">History for All Members</h3>
				
				<table className="table table-bordered table-striped table-dark table-responsive-sm">
						
					<thead>
						<tr className="text-center">
							<th>User Name</th>
							<th>Order ID</th>
							<th>Item Name</th>
							<th>Date Created</th>
							<th>Approval Status</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						{ 
							this.state.rentalalls.map((rentalall) => {
								return <tr key="rentalall.rental_id" className="text-center">
									<td>{ rentalall.name} </td>
									<td>{ rentalall.rental_id }</td>
									<td>{ rentalall.item_name } </td>
									<td>{ rentalall.created_at }</td>
									<td>{ 
										(rentalall.admin_approved==0) ? (<i>Pending Approval</i>) :
										(rentalall.admin_approved==1) ? (<div className="text-success">Approved - Now Using</div>) :
										(rentalall.admin_approved==2) ? (<div className="text-danger">Disapproved</div>) :
										(rentalall.admin_approved==3) ? (<div className="text-primary">Returned by Member</div>) :
										(rentalall.admin_approved==4) ? (<div className="text-warning font-italic">Pending Return</div>) :
										(<p>Request Invalid</p>) 
									}</td>
									<td>
										{ (rentalall.admin_approved==0) ?
												( <div>
													<button onClick={ () => this.approve(rentalall.rental_id) } className="btn btn-success mx-2" id="history-btn">Approve</button>
											   	<button onClick={ () => this.deny(rentalall.rental_id) } className="btn btn-danger mx-2" id="history-btn">Reject</button>
												</div> ) :
											(rentalall.admin_approved==4 || rentalall.admin_approved==1) ? 
												( <div>
													<button onClick={ () => this.tagReturned(rentalall.rental_id) } className="btn btn-light mx-2" id="history-btn">Tag as Returned</button>
												  <button onClick={ () => this.deny(rentalall.rental_id) } className="btn btn-danger mx-2" id="history-btn">Suspend Use</button>
												</div> ) :
											(<i>Action Completed</i>)
										}
									</td>
								</tr>
							})
						}
					</tbody>

				</table>
				<br/><br/>

			</div>
		)
	}
}


export default History