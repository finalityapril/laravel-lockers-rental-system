import React, { Component, Fragment } from 'react'
import { Redirect, Link } from 'react-router-dom'

const AddItem = () => (
	<div className="container-fluid mt-3">
		<div className="row">
			<div className="col-6 mx-auto">
				<h3 className="text-center mt-5" id="header-text">Add Locker</h3>
				<div className="card">
					<div className="card-header">Locker Information</div>
					<div className="card-body">

						<AddItemForm/>

					</div>
				</div>
			</div>
		</div>
		<br/><br/>
	</div>
)

class AddItemForm extends Component {

constructor(props) {
	super(props)

	this.fileInput =  React.createRef();

	this.state = {
		itemName: '',
		description:'',
		categoryId: undefined,
		categories:[],
		statusId: undefined,
		statuses:[],
		returnToMenu:false
	}
}


componentWillMount() {
	fetch('/api/categories')
	.then((response) => response.json())
	.then((categories)=> {
		this.setState({categories: categories})
	});

	fetch('/api/statuses')
	.then((response) => response.json())
	.then((statuses)=> {
		this.setState({statuses: statuses})
	})
}

// componentDidMount() {
// 	console.log('Component did mount')
// }

// componentDidUpdate() {
// 	console.log('Component did update')
// }

itemNameChangeHandler(e){
	this.setState({itemName: e.target.value})
}
descriptionChangeHandler(e){
	this.setState({description: e.target.value})
}
categoryIdChangeHandler(e){
	this.setState({categoryId: e.target.value})
}
statusIdChangeHandler(e){
	this.setState({statusId: e.target.value})
}


formSubmitHandler(e){

	e.preventDefault();

	let formData = new FormData()
	
	formData.append('item_name', this.state.itemName)
	formData.append('description', this.state.description)
	formData.append('image_location', this.fileInput.current.files[0])
	formData.append('category_id', this.state.categoryId)
	formData.append('status_id', this.state.statusId)

	let payload = { 
		method: 'post', 
		body: formData 
	}

	fetch('/api/item', payload)
	.then((response) => response.json())
	.then((data) => {
		this.setState({
			returnToMenu: true
		})
	})

}

render() {
	//Condition to check if returnToMenu is true 
	if(this.state.returnToMenu){
		return <Redirect to='/menu'/>
	}


	return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				<div className="form-group">
					<label>Item Name</label>
					<input value={this.state.itemName} type="text" onChange={ this.itemNameChangeHandler.bind(this) } className="form-control" required/>
				</div>

				<div className="form-group">
					<label>Description</label>
					<input value={this.state.description} type="text" onChange={ this.descriptionChangeHandler.bind(this) } className="form-control" required/>
				</div>

				<div className="form-group">
					<label>Category</label>
					<select defaultValue className="form-control" value={this.state.categoryId} onChange={ this.categoryIdChangeHandler.bind(this) }>
						<option>Select Category</option>
							{
								this.state.categories.map((category) => {
									return <option key={ category.id } value={ category.id }>{ category.category_name }</option>
								})
							}
					</select>
				</div>

				<div className="form-group">
					<label>Status</label>
					<select defaultValue className="form-control" value={this.state.statusId} onChange={ this.statusIdChangeHandler.bind(this) }>
						<option>Select Status</option>
							{
								this.state.statuses.map((status) => {
									return <option key={ status.id } value={ status.id }>{ status.status_name }</option>
								})
							}
					</select>
				</div>

				<div className="form-group">
					<label>Image</label>
					<input type="file" ref={ this.fileInput } className="form-control" required/>
				</div>

				<button type="submit" className="btn btn-success btn-block">Add</button>
				<Link className="btn btn-danger btn-block" to="/menu">Cancel</Link>

			</form>
		)
	}
}

export default AddItem