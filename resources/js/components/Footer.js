import React from 'react'

const Footer = () => {
	return(
		<footer className="bg-secondary fixed-bottom" id="footer-style">
			<p className="text-center py-2 text-white my-0">
				Copyright &copy; 2019 LockersCo
			</p>
		</footer>
	)
}

export default Footer