import React, { Component } from 'react'
import { Link } from 'react-router-dom'

const Header = (props) => {

	let navRight = (props.id == null) ?
		(
			<React.Fragment>
			<li className="nav-item">
				<Link className="nav-link" to="/login">Login</Link>
			</li>

			</React.Fragment>
		) : 
		(
			<React.Fragment>
			<li className="nav-item">
				<Link className="nav-link text-warning" to="/">{ props.name }</Link>
			</li>
			
			{ (props.role == 'admin') ?
				(<li className="nav-item"> 
					<Link className="nav-link" to="/history">History Panel</Link>
				</li>) :
				(props.role == 'member') ?
				(<li className="nav-item"> 
					<Link className="nav-link" to="/transactions">Transactions</Link>
				</li>) :
				(<div></div>)
			}
			<li className="nav-item">
				<Link className="nav-link" to="/logout">Logout</Link>
			</li>
			</React.Fragment>
		)

	let navLeft = (props.id == null) ?
		( <React.Fragment>
				<li className="nav-item">
					<Link className="nav-link" to="/">About</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" to="/menu">Menu</Link>
				</li>
			</React.Fragment>
		) : (props.role=='admin') ?
		(	<React.Fragment>
				<li className="nav-item">
					<Link className="nav-link" to="/">About</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" to="/menu">Menu</Link>
				</li>
				
				<li className="nav-item">
					<Link className="nav-link" to="/register">Register</Link>
				</li>

				<li className="nav-item">
					<Link className="nav-link" to="/userlist">Users</Link>
				</li>
			</React.Fragment>
		) :
		(
			<React.Fragment>
				<li className="nav-item">
					<Link className="nav-link" to="/">About</Link>
				</li>
				<li className="nav-item">
					<Link className="nav-link" to="/menu">Menu</Link>
				</li>
				
				<li className="nav-item">
					<Link className="nav-link" to="/assets">Requests <span className="badge badge-light">{ props.assetsQuantity }</span></Link>
				</li>
			</React.Fragment>
		)

	return(
		<nav className="navbar navbar-expand-md navbar-dark bg-secondary">
		
			<Link className="navbar-brand mx-2" to="/" id="logo-text1">Pop<span className="text-white">Lockers</span></Link>

			<button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar">
				<span className="navbar-toggler-icon"></span>
			</button>

			<div className="collapse navbar-collapse" id="navbar">

				<ul className="navbar-nav mr-auto">

					{ navLeft }

				</ul>

				<ul className="navbar-nav ml-auto">

					{ navRight }

				</ul>

			</div>

		</nav>
	)
}

export default Header