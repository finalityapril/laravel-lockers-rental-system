import React, { Component } from 'react'
import { Link } from 'react-router-dom'

const Menu = (props) => {

	let btnAddItem = (props.user.role == 'admin') ?
	// ternary operator execution
	<Link className="btn btn-sm btn-primary" to="/add-item">Add Item</Link> : null

	return(
		<div className="container-fluid mt-3">
 
			<h3 id="header-text">Available Lockers</h3>

			{ btnAddItem }

			<MenuList role={ props.user.role } getTotalAssetsQuantity={ props.getTotalAssetsQuantity }/>

		</div>
	)
	
}


class MenuList extends Component {

	constructor(props) {
		super(props)

		this.state = {
			items: [],
			categoryId: undefined,
			categories:[],
			categoryName: ''
		}
	}

	componentWillMount() {
		fetch('/api/items')
		.then((response) => response.json())
		.then((items) => {
			this.setState({ items: items })
		})
	}

	render() {

		return (

			<div className="row mt-3">
			{
				this.state.items.map((item) => {
					return (
						<div key={ item.id } className="col-sm-3 col-md-4 col-lg-3 mb-3">
							
							<div className="card" id="menu-card">

								<img className="card-img-top" width="100px" height="150px" style={ { objectFit:'cover' } } src={ item.image_location }/>

								<div className="card-body">

									<h4 className="card-title">{ item.item_name }</h4>

									<p className="card-text">{ item.description }</p>
									
									{
										(item.category_id == 1) ? ( <p>Small </p> ):
										(item.category_id == 2) ? ( <p> Medium </p> ):
										(item.category_id == 3) ? (<p> Large </p>) : (<p> </p>)
									}
											
									{
										(this.props.role == 'admin') ?
										(
											<div className="btn-group btn-block">
												<Link className="btn btn-primary" to={ "/edit-item?id=" + item.id }>Edit</Link>
												<Link className="btn btn-danger" to={ "/delete-item?id=" + item.id }>Delete</Link>
											</div>
										):
										(this.props.role == 'member') ?
										(   ( localStorage.getItem('now_booking') == 1) ?
												  ( <Link className="btn btn-secondary" to={"/assets"}>Please finish or cancel current booking. </Link> ) :
											(<Link className="btn btn-primary" to={ "/request-item?id=" + item.id}> Request </Link>)
										):
										(   <div className="btn-group btn-block"> 
												<Link className="btn btn-warning" to={"/login"}>Login to Request Item </Link>
											</div> 
										)
										
									}
	
								</div>

							</div>

						</div>
					)
				})
			}
			</div>
		)
	}

}


export default Menu