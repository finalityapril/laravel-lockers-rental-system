import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom'

const Register = () => (

	<div className="container-fluid mt-3">

		<div className="row">

			<div className="col-sm-6 mx-auto">

				<h3 className="text-center mt-5" id="header-text">Register New User</h3>

				<div className="card">

					<div className="card-header">Register Information</div>

					<div className="card-body">

						<RegisterForm/>

					</div>
				</div>
			</div>
		</div>
		<br/> <br/>
	</div>
	)

class RegisterForm extends Component {

	constructor(props){
		super(props)

		this.state = {
			name: '',
			email:'',
			password:'',
			gotoLogin: false,
			isSubmitDisabled: true
		}
	}

	nameChangeHandler(e) {
		this.setState({
			name: e.target.value
		})
	}

	emailChangeHandler(e) {
		this.setState({
			email: e.target.value
		})
	}

	passwordChangeHandler(e) {

		if(e.target.value.length < 8) {
			this.setState({ isSubmitDisabled: true })
		} else {
			this.setState({ isSubmitDisabled: false })
		}

		this.setState({
			password: e.target.value
		})

	}

	formSubmitHandler(e) {
		e.preventDefault()

		let formData = new FormData()

		formData.append('name', this.state.name)
		formData.append('email', this.state.email)
		formData.append('password', this.state.password)

		let payload = {
			method: 'post',
			body: formData
		}
		//.then(function(response) { return response.json() }) - old version for writing
		fetch('/api/register', payload)
		.then((response) => response.json())
		.then((response) => {
			this.setState({ gotoLogin: true })
		})
	}

	render() {

		if(this.state.gotoLogin == true) {
			return <Redirect to='/userlist'/>
		}

		return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				<div className="form-group">

					<label>Name</label>
					<input value={ this.state.name } onChange={ this.nameChangeHandler.bind(this) } type="text" className="form-control" />
					<span className="text-danger"></span>

				</div>

				<div className="form-group">

					<label>Email</label>
					<input value={ this.state.email } onChange={ this.emailChangeHandler.bind(this) } type="email" className="form-control mb-1"/>
					<span className="text-danger"></span>

				</div>

				<div className="form-group">

					<label>Password</label>
					<input value={ this.state.password } onChange={ this.passwordChangeHandler.bind(this) } type="password" className="form-control mt-1"/>
					<span className="text-danger"></span>

					<button disabled={ this.state.isSubmitDisabled } type="submit" className="btn btn-success btn-block mt-3">Register</button>
					<Link className="btn btn-danger btn-block" to="/userlist">Cancel</Link>

				</div>

			</form>
			)
	}

}

export default Register