import React, { Component } from 'react'
import queryString from 'query-string'
import { Redirect } from 'react-router-dom'

const Login = (props) => {

	if(props.id != null) {
		return <Redirect to='/menu'/>
	}

	return(
		<div className="container-fluid mt-3">

			<div className="row">
					
					<div className="col-sm-6 mx-auto">
						
						<h3 className="text-center mt-5" id="header-text">Login</h3>

						<div className="card">
							
							<div className="card-header">Enter Login Information</div>

							<div className="card-body">

								<LoginForm urlParam={ props.location.search } setUser={ props.setUser } />

							</div>

						</div>

					</div>

			</div>

		</div>
	)
	
}

class LoginForm extends Component {

	constructor(props) {
		super(props)

		this.state = {
			email: '',
			password: '',
			errorMessage: ''
		}
	}

	emailChangeHandler(e) {
		this.setState({ email: e.target.value })
	}

	passwordChangeHandler(e) {
		this.setState({ password: e.target.value })
	}

	formSubmitHandler(e) {
		e.preventDefault()

		// console.log(this.state)
		let formData = new FormData()
		formData.append('email', this.state.email)
		formData.append('password', this.state.password)

		let payload = { method: 'post', body: formData }

		fetch('/api/login', payload)
		.then((response) => response.json())
		.then((response) => {
			if(response.result == 'authenticated') {
				this.props.setUser(response.user)
			} else if (response.result == 'not-found') {
				this.setState({
					errorMessage: <div className="alert alert-danger">Credentials not found, please try again.</div>
				})
			} else if (response.result == 'denied') {
				this.setState({
					errorMessage: <div className="alert alert-danger">Password is incorrect, please try again.</div>
				})
			}
		})
	}

	render() {
		
		//url turn into object
		let url = this.props.urlParam
		let params = queryString.parse(url)
		let registerSuccessMessage = null
		let message = null
		
		if(params.register_success) {
			registerSuccessMessage = <div className="alert alert-success">Registration successful, you may now login.</div>
		}

		if (this.state.errorMessage == '' && registerSuccessMessage != null) {
			message = registerSuccessMessage
		} else {
			message = this.state.errorMessage
		}

		return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				{ message }
								
				<label>Email</label>
				<input value={ this.state.email } onChange={ this.emailChangeHandler.bind(this) } type="text" className="form-control mb-2"/>

				<label>Password</label>
				<input value={ this.state.password } onChange={ this.passwordChangeHandler.bind(this) } type="password" className="form-control mb-3"/>
	
				<button type="submit" className="btn btn-success btn-block">Login</button>

			</form>
		)
	}

}

export default Login