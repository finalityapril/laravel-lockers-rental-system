import React, { Component } from 'react'
import queryString from 'query-string'
import { Link, Redirect } from 'react-router-dom'


const RequestItem = (props) => (

	<div className="container-fluid mt-3">

	        <div className="row">

	            <div className="col-sm-6 mx-auto">

	                <h3 className="text-center mt-5" id="header-text">Request Item</h3>

	                <div className="card">

	                    <div className="card-header">Item Information</div>

	                    <div className="card-body">

	                    	<RequestItemForm urlParam = { props.location.search } getTotalAssetsQuantity={ props.getTotalAssetsQuantity}/>

	                    </div>

	                </div>

	            </div>
	        </div>
	        <br/><br/><br/>
	    </div>
)

class RequestItemForm extends Component {

	constructor(props) {
		super(props)

		let params = queryString.parse(this.props.urlParam)

		this.state = {
			itemId: params.id,
			itemName: '',
			description:'',
			categoryId: undefined,
			categories:[],
			statusId: undefined,
			statuses: [],
			quantity: 1,
			returnToAssetPage:false,
			startDate: '',
			endDate: ''
		}
	}

	componentWillMount() {
		fetch('/api/item/' + this.state.itemId)
		.then((response) => response.json())  
		.then((item) => {
			this.setState({ 
			 itemName: item.item_name,
			 description: item.description,
			 categoryId: item.category_id,
			 statusId: item.status_id,
			})
		})

		fetch('/api/categories')
		.then((response) => response.json())
		.then((categories)=> {
			this.setState({categories: categories})
		})

		fetch('/api/statuses')
		.then((response) => response.json())
		.then((statuses)=> {
		this.setState({statuses: statuses})
		})
	}

	quantityChangeHandler(e) {
		this.setState({ quantity: e.target.value })
	}

	formSubmitHandler(e) {
		e.preventDefault()

		let formData = new FormData()
		formData.append('item_id', this.state.itemId)
		formData.append('quantity', this.state.quantity)
		formData.append('startDate', this.state.startDate)
		formData.append('endDate', this.state.endDate)


		let payload = { 
			method: 'post', 
			body: formData,
			headers: {
				'X-Requested-With': 'XMLHttpRequest',
				'X-CSRF-TOKEN': document.head.querySelector('meta[name=csrf-token]').getAttribute('content')
			}
		}

		fetch('/assets/add', payload)
		.then((response) => response.text())
		.then((data) => {
			this.props.getTotalAssetsQuantity()
			this.setState({ quantity: 0 })
			this.setState({returnToAssetPage: true})
			alert('Item has been added to requests.')
			
		})

	}

	render() {

		if(this.state.returnToAssetPage){
			return <Redirect to='/assets'/>
		}

		return (

		<form onSubmit={ this.formSubmitHandler.bind(this) }>

			<div className="form-group">
				<label>Item Name</label>
				<input value={this.state.itemName} type="text"  className="form-control" readOnly/>
			</div>

			<div className="form-group">
				<label>Description</label>
				<input value={this.state.description} type="text"  className="form-control" readOnly/>
			</div>

			<div className="form-group">
				<label>Category</label>
				{
					(this.state.categoryId == 1) ? ( <input value='Small' type="text" className="form-control" readOnly/> ) :
					(this.state.categoryId == 2) ? ( <input value='Medium' type="text" className="form-control" readOnly/> ):
					(this.state.categoryId == 3) ? ( <input value='Medium' type="text" className="form-control" readOnly/> ):
					(<p> </p>)
				}
			</div>
			
			<button type="submit" className="btn btn-success btn-block">Add</button>
			<Link className="btn btn-danger btn-block" to="/menu">Cancel</Link>

		</form>

		)
	}

}


export default RequestItem

