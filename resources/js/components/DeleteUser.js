import React, { Component } from 'react'
import queryString from 'query-string'
import { Link, Redirect } from 'react-router-dom'

const DeleteUser = (props) => (

	<div className="container-fluid mt-3">

        <div className="row">

            <div className="col-sm-6 mx-auto">

                <h3 className="text-center mt-5" id="header-text">Delete User</h3>

                <div className="card">

                    <div className="card-header">User Information</div>

                    <div className="card-body">

                    	<DeleteUserForm urlParam={ props.location.search }/>

                    </div>
                </div>
            </div>
        </div>
    </div>

)

class DeleteUserForm extends Component {
	
	constructor(props) {
		super(props)

		let params = queryString.parse(this.props.urlParam)

		this.state = {
			userId: params.id,
			userName: '',
			email:'',
			userRole: ''
		}
	}

	componentWillMount() {
		fetch('/api/user/' + this.state.userId)
		.then((response) => response.json())  
		.then((user) => {
			this.setState({ 
			 userName: user.name,
			 email: user.email,
			 userRole: user.user_role
			})
		})
	}

	userNameChangeHandler(e){
		this.setState({userName: e.target.value})
	}
	emailChangeHandler(e){
		this.setState({email: e.target.value})
	}

	userRoleChangeHandler(e){
		this.setState({userRole: e.target.value})
	}

	formSubmitHandler(e){

	e.preventDefault();
	// create new form
	let formData = new FormData()
	// send through existing form to states
	formData.append('_method', 'DELETE')
	formData.append('id', this.state.userId)

	let payload = { 
		method: 'post', 
		body: formData 
	}

	fetch('/api/user', payload)
	.then((response) => response.text())
	.then((data) => {
		this.setState({
			returnToList: true
		})
	})

}

render() {

	if(this.state.returnToList){
		return <Redirect to='/userlist'/>
	}

	return (
			<form onSubmit={ this.formSubmitHandler.bind(this) }>

				<div className="form-group">
					<label>User Name</label>
					<input value={this.state.userName} type="text" onChange={ this.userNameChangeHandler.bind(this) } className="form-control" readOnly/>
				</div>

				<div className="form-group">
					<label>Email</label>
					<input value={this.state.email} type="text" onChange={ this.emailChangeHandler.bind(this) } className="form-control" readOnly/>
				</div>

				<div className="form-group">
					<label>User Role</label>
					<input value={this.state.userRole} type="text" onChange={ this.userRoleChangeHandler.bind(this) } className="form-control" readOnly/>
				</div>

				<button type="submit" className="btn btn-danger btn-block">Delete</button>
				<Link className="btn btn-secondary btn-block" to="/userlist">Cancel</Link>

			</form>
		)
	}

}

export default DeleteUser