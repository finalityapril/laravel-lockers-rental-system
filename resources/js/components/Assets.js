import React, { Component } from 'react'
import { Redirect,Link } from 'react-router-dom'

class Assets extends Component {

	constructor(props) {
		super(props)

		this.state = {
			items: [],
			gotoTransactions: false
		}
	}

	componentWillMount() {
		this.getItems()
	}

	getItems() {
		fetch('/assets/info')
		.then((response) => response.json())
		.then((requestsItems) => {
			console.log(requestsItems)
			this.setState({ items: requestsItems })
		})
	}


	emptyAssets() {
		this.setState({ items: [] })
		this.props.emptyAssets()
	}

	proceedToCheckout() {
		let formData = new FormData()

        formData.append('user_id', this.props.userId)

        let payload = { 
            method: 'post', 
            body: formData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': document.head.querySelector('meta[name=csrf-token]').getAttribute('content')
            }
        }
        fetch('/assets/checkout', payload)
        .then((response) => response.text())
        .then((response) => {
        	console.log(response)
           this.setState({ gotoTransactions: true })
        })
        this.emptyAssets()  
       
	}


	render() {
		if(this.state.gotoTransactions) {
			return <Redirect to='/transactions'/>
		}

		if(this.state.items.length == 0) {
			localStorage.setItem('now_booking', 0)
			return (
				<div className="container-fluid mt-3">
					<h3>No items in requests.</h3>
					<h4>Select an item to add to from <Link to="/menu" id="empty-link">here</Link></h4>
				</div>
			)
		}

		let totalQuantity = 0

		localStorage.setItem('now_booking', 1)
		this.state.items.map((item) => {
			totalQuantity += item.quantity
		})

		return (
			<div className="container-fluid mt-3 col-sm-8 mx-auto">

				<h3 id="header-text" className="text-center">Request</h3>

				<table className="table table-bordered table-striped table-dark">

					<thead>

						<tr>
							<th>Item</th>
							<th>Quantity</th>
						</tr>

					</thead>

					<tbody>
						
						<AssetsList totalRentals={ this.state.rentals } getTotalAssetsQuantity={ this.props.getTotalAssetsQuantity } getItems={ this.getItems.bind(this) } items={ this.state.items }/>
						<tr>
							<td className="text-right"><b>Total Quantity: </b></td>
							<td className="text-left"><b>{ totalQuantity }</b></td>
						
							
						</tr>
					</tbody>

				</table>

				<button className="btn btn-danger" onClick={ this.emptyAssets.bind(this) }>Empty Requests</button>&nbsp;
				<button className="btn btn-primary" onClick={ this.proceedToCheckout.bind(this) }>Send for Approval</button>&nbsp;

			</div>
		)
	}
}

const AssetsList = (props) => {

	return (
		props.items.map((item) => {
			return (
				<tr key={ item.id }>
					<td>{ item.item_name }</td>
					<td>
						<UpdateItemInput getTotalAssetsQuantity={ props.getTotalAssetsQuantity } getItems={ props.getItems } itemId={ item.id } quantity={ item.quantity } />
					</td>
				</tr>
			)
		})
	)
}


const RemoveItemButton = (props) => {

	const removeItem = () => {
		fetch('/assets/remove-item/' +props.itemId)
		.then((response) => response.text())
		.then(() => {
			props.getTotalAssetsQuantity()
			props.getItems()
		})
	}

	return (
		<button onClick={ removeItem } className="btn btn-danger">Remove</button>
	)

}

const UpdateItemInput = (props) => {

	const updateQuantity = (e) => {
		let formData = new FormData()

        formData.append('item_id', props.itemId)
        formData.append('quantity', e.target.value)

        let payload = { 
            method: 'post', 
            body: formData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': document.head.querySelector('meta[name=csrf-token]').getAttribute('content')
            }
        }

        fetch('/assets/update-item-quantity', payload)
        .then((response) => response.text())
        .then(() => {
            props.getTotalAssetsQuantity()
            props.getItems()
        })
	}

	return (
		<input value={ props.quantity } onChange={ updateQuantity } type="number" className="form-control" min="1" readOnly/>
	)

}

export default Assets