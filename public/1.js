(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/appstyle.css":
/*!****************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/src??ref--6-2!./resources/appstyle.css ***!
  \****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.push([module.i, "@import url(https://fonts.googleapis.com/css?family=Bangers&display=swap);", ""]);

// module
exports.push([module.i, ".navbar {\r\n\tbackground: #333333 !important;\r\n}\r\n\r\n#footer-style {\r\n\tbackground: #333333 !important;\r\n}\r\n\r\nbody {\r\n\tbackground-color: #989898;\r\n\tcolor: white;\r\n}\r\n\r\n#header-text {\r\n\tfont-family: 'Bangers', cursive;\r\n\tletter-spacing: 0.1rem;\r\n\t/*-webkit-text-stroke: 1px black; */\r\n   color: #F0cd13;\r\n   text-shadow:\r\n       3px 3px 0 #000,\r\n     -1px -1px 0 #000,  \r\n      1px -1px 0 #000,\r\n      -1px 1px 0 #000,\r\n       1px 1px 0 #000;\r\n}\r\n\r\n#logo-text1 {\r\n\tfont-family: 'Bangers';\r\n\tletter-spacing: 0.09rem;\r\n\tcolor: #EC3C37;\r\n}\r\n#logo-text2 {\r\n\tfont-family: 'Bangers';\r\n\tletter-spacing: 0.09rem;\r\n\tcolor: #ffffff;\r\n}\r\n\r\n#user-text {\r\n\tfont-family: 'Bangers';\r\n\tletter-spacing: 0.09rem;\r\n\tcolor: #f2a28d;\r\n}\r\n\r\n#empty-link {\r\n\tcolor: yellow !important;\r\n}\r\n\r\ntable {\r\n\tcolor: white !important;\r\n}\r\n.card {\r\n\tbackground: rgba(0, 0, 0, 0.6) !important;\r\n\tborder-width: 0.3rem;\r\n\tborder-style: dashed;\r\n\tborder-color: black;\r\n}\r\n\r\n\r\n\r\ntable {\r\n\tbackground: rgba(0, 0, 0, 0.6) !important;\r\n}", ""]);

// exports


/***/ }),

/***/ "./resources/appstyle.css":
/*!********************************!*\
  !*** ./resources/appstyle.css ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader??ref--6-1!../node_modules/postcss-loader/src??ref--6-2!./appstyle.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/appstyle.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);