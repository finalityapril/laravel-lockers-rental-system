<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('/categories', function() {
// 	// DB::table('categories')->get();
// 	// if($_SERVER['REMOTE_ADDR'] != 'ip-address') {
// 	// 	return 'FORBIDDEN ACCESS';
// 	// }
// 	return Category::all();
// });


// [SECTION] Routes for Items.

Route::get('/items', 'ItemController@all');

Route::get('/item/{id}', 'ItemController@find');

Route::post('/item', 'ItemController@create');

Route::put('/item', 'ItemController@update');

Route::delete('/item', 'ItemController@delete');


// [SECTION] Routes for Categories.

Route::get('/categories', 'CategoryController@all');


// [SECTION] Routes for Statuses.
Route::get('/statuses', 'StatusController@all');



// [SECTION] Routes for Orders.
Route::get('/rental/{id}', 'RentalController@all');
Route::get('/rentals', 'RentalController@retrieveall');
Route::post('/rental', 'RentalController@create');

Route::put('/rentalok', 'RentalController@approve');
Route::put('/rentalno', 'RentalController@deny');
Route::put('/rentalreturn', 'RentalController@returned');
Route::put('/forreturn', 'RentalController@forreturn');

// [SECTION] Routes for Users.

Route::post('/login', function(Request $request) {
	$user = App\User::where('email', '=', $request->email)->first();

	if($user == null) {
		return json_encode(array('result' => 'not-found'));
	} else {
		if(Auth::attempt($request->only('email', 'password'))) {
			return json_encode(array('result' => 'authenticated', 'user' => $user));
		} else {
			return json_encode(array('result' => 'denied'));
		}
	}
});

Route::get('/users', 'UserController@all');

Route::get('/admins', 'UserController@admins');

Route::get('/user/{id}', 'UserController@find');

Route::put('/user', 'UserController@update');

Route::put('/useractivate', 'UserController@reactivate');

Route::delete('/user', 'UserController@delete');

//[SECTION] Routes for Registration

Route::post('/register', function(Request $request) {

	return App\User::create([
		'name' => $request->name,
		'email' => $request->email,
		'password' => Hash::make($request->password),
		'user_role' => 'member',
		'now_renting' => 0
	]);
});



