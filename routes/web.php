<?php

use Illuminate\Http\Request;
use App\Item;
use App\Rental;
use App\RentalItem;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// any path for visual go to react
Route::view('/{path?}', 'app');

// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');

// [SECTION] Routes for assets.

Route::post('/assets/add', function(Request $request) {
	// $request->session()->put('assets', $request->quantity);
	$key = 'assets.'.$request->item_id;
	$quantity = intval($request->quantity);
	$startDate = date($request->startDate);
	$endDate = date($request->endDate);

	if($request->session()->has($key)) {
		$quantity += $request->session()->get($key);
		$request->session()->put($key, $quantity);
	} else {
		$request->session()->put($key, $quantity);
	}

	return $request->session()->get('assets');

});

Route::get('/assets/total-quantity', function(Request $request) {
	$total_quantity = 0;
	// if may laman ang assets mag-loop tayo
	if ($request->session()->has('assets')) {
		foreach($request->session()->get('assets') as $item_id => $quantity) {
			$total_quantity += $quantity;
		}
	}
	return $total_quantity;
});


Route::get('/assets/empty', function(Request $request) {
	$request->session()->forget('assets');
});

Route::get('/assets/remove-item/{id}', function(Request $request, $id) {
	$request->session()->forget('assets.'.$id);
});

Route::get('/assets/info', function(Request $request) {
	$assets_items = $request->session()->get('assets');
	$assets_data = [];
	
	
	if ($request->session()->has('assets')) {
		foreach ($assets_items as $item_id => $quantity) {
			$item = Item::find($item_id);

			$item_data = array(
				'id' => $item_id,
				'item_name' => $item->item_name,
				'quantity' => $quantity,					
			);

			array_push($assets_data, $item_data);
		}
	}

	return json_encode($assets_data);
});

Route::post('/assets/update-item-quantity', function(Request $request) {
	$key = 'assets.'.$request->item_id;
	$quantity = intval($request->quantity);

	if($quantity == 0) {
		$request->session()->forget('assets.'.$request->item_id);
	} else {
		$request->session()->put($key, $quantity);
	}

});

Route::post('/assets/checkout', function(Request $request) {
	$user_id = $request->user_id;
	$assets_items = $request->session()->get('assets');

	if(!empty($assets_items)) {
		foreach($assets_items as $item_id => $quantity) {
			$item = Item::find($item_id);
			// previous subtotal
			
		}
	}

	$rental = Rental::create([
		'user_id' => $user_id,
		'created_at' => Carbon\Carbon::now()
	]);

	foreach($assets_items as $item_id => $quantity) {
		RentalItem::create([
			'rental_id' => $rental->id,
			'item_id' => $item_id,
			'quantity' => $quantity,
			'admin_approved' => 0,
			'is_archived' => 0
		]);
	}

});



