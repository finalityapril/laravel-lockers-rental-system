<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Category::create(['category_name'=>'Small']);
        Category::create(['category_name'=>'Medium']);
        Category::create(['category_name'=>'Large']);
    }
}
