<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Status::create(['status_name'=>'Available']);
    	Status::create(['status_name'=>'In Use']);
    	Status::create(['status_name'=>'Pending']);
    	Status::create(['status_name'=>'For Repair']);
    }
}
