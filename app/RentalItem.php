<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RentalItem extends Model
{
    protected $fillable = ['rental_id', 'item_id', 'quantity', 'admin_approved'];
    public $timestamps = false;
}
