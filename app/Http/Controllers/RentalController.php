<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rental;
use App\RentalItem;
use Carbon;

class RentalController extends Controller
{
		public function all($user_id) {
			return Rental::where('user_id', $user_id)
			->select('rentals.id', 'rentals.user_id', 'rentals.created_at', 'rental_items.id', 'rental_items.rental_id','rental_items.item_id', 'rental_items.quantity', 'rental_items.admin_approved', 'rental_items.is_archived', 'users.id', 'users.name', 'items.id', 'items.item_name')
			->join('rental_items','rental_items.rental_id', '=', 'rentals.id')
			->join('users','users.id','=','rentals.user_id')
			->join('items','items.id','=','rental_items.item_id')
			->orderBy('rental_items.rental_id', 'DESC')
			->get();

		}

		public function retrieveall() {
			return Rental::select('rentals.id', 'rentals.user_id', 'rentals.created_at', 'rental_items.id', 'rental_items.rental_id','rental_items.item_id', 'rental_items.quantity', 'rental_items.admin_approved', 'rental_items.is_archived', 'users.id', 'users.name', 'items.id', 'items.item_name')
			->join('rental_items','rental_items.rental_id', '=', 'rentals.id')
			->join('users','users.id','=','rentals.user_id')
			->join('items','items.id','=','rental_items.item_id')
			->orderBy('rental_items.rental_id', 'DESC')
			->get();
		}

		public function create(Request $request) {
			$rental = Rental::create([
			'user_id' => $request->user_id,
			'created_at' => Carbon\Carbon::now()
		]);

			foreach($request->items as $item) {
				RentalItem::create([
					'rental_id' => $rental['user_id'],
					'item_id' => $item['id'],
					'quantity' => $item['quantity'],
					'admin_approved' => 0
				]);
			}
		 }

		 public function approve(Request $request) {
			$rentalapprove=RentalItem::where('rental_id', $request->rental_id)
			->update(['admin_approved' => 1]);
			
			return $rentalapprove;
		}

		public function deny(Request $request) {
			$rentaldeny=RentalItem::where('rental_id', $request->rental_id)
			->update(['admin_approved' => 2]);
			
			return $rentaldeny;
		}

		public function returned(Request $request) {
			$rentalreturn=RentalItem::where('rental_id', $request->rental_id)
			->update(['admin_approved' => 3]);
			
			return $rentalreturn;
		}

		public function forreturn(Request $request) {
			$rentalreturn=RentalItem::where('rental_id', $request->rental_id)
			->update(['admin_approved' => 4]);
			
			return $rentalreturn;
		}

		public function delete(Request $request) {
			Rental::where('id', $request->id)->update(['is_archived' => 1]);
		}
}
