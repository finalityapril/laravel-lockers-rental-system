<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\User;


class UserController extends Controller
{
    public function all() {
    	return User::where('user_role', 'member')
        ->get();
    }

    public function admins() {
        return User::where('user_role', 'admin')
        ->get();
    }

    public function find($id) {
    	return User::find($id);
    }

    public function create(Request $request) {

        return User::create([
        'name' => $request->name,
        'email' => $request->email,
        'user_role' => $request->user_role,
        'is_archived' => 0,
        'now_renting' => 0
        ]);
    }

    public function update(Request $request) {
    	$user = User::find($request->id);

		$user->name = $request->name;
		$user->email = $request->email;
		$user->user_role = $request->user_role;
        
		$user->save();

		return $user;
    }

    public function delete(Request $request) {
    	User::where('id', $request->id)->update(['is_archived' => 1]);
    }

    public function reactivate(Request $request) {
        User::where('id', $request->id)->update(['is_archived' => 0]);
    }

}
