<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function all() {
    	return Category::all();
    }

    public function find($id) {
    	return Category::find($id);
    }
}
