<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Item;


class ItemController extends Controller
{
    public function all() {
    	return Item::where('is_archived', 0)->get();
    }

    public function find($id) {
    	return Item::find($id);
    }

    public function create(Request $request) {

        $path = $request->file('image_location')->store('/items', ['disk' => 'public']);

    	return Item::create([
		'item_name' => $request->item_name,
		'description' => $request->description,
		'image_location' => $path,
		'category_id' => $request->category_id,
        'status_id' => $request->status_id,
        'is_archived' => 0,
		]);
    }

    public function update(Request $request) {
    	$item = Item::find($request->id);

		$item->item_name = $request->item_name;
		$item->description = $request->description;
		$item->category_id = $request->category_id;
        $item->status_id=$request->status_id;

        if($request->hasFile('image_location')) {
            Storage::disk('public')->delete($item->image_location);

            $path = $request->file('image_location')->store('/items', ['disk' => 'public']);
            $item->image_location = $path;

        }

		$item->save();

		return $item;
    }

    public function delete(Request $request) {
    	Item::where('id', $request->id)->update(['is_archived' => 1]);
    }

}
