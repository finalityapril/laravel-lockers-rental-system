<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{

    protected $fillable = ['user_id', 'created_at'];
    public $timestamps = false;

}
